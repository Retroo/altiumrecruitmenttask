﻿using System;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Application.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace AltiumRecruitmentTask.Infrastructure
{
    public class StringDotNumberComparer : IStringDotNumberComparer
    {
        private readonly AppSettings _appSettings;

        public StringDotNumberComparer(IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
        }

        public int Compare(string stringA, string stringB)
        {
            var delimiter = _appSettings.File.RecordsData.Delimiter;
            var aStringPart = stringA.Split(delimiter)[1];
            var bStringPart = stringB.Split(delimiter)[1];

            var aNumberPart = int.Parse(stringA.Split(delimiter)[0]);
            var bNumberPart = int.Parse(stringB.Split(delimiter)[0]);

            var stringComparisonResult = string.Compare(aStringPart, bStringPart, StringComparison.CurrentCulture);

            switch (stringComparisonResult)
            {
                case < 0:
                case > 0:
                    return stringComparisonResult;
                case 0:
                    return aNumberPart - bNumberPart;
            }
        }
    }
}
