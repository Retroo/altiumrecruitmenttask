﻿using System;
using System.Text;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Application.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace AltiumRecruitmentTask.Infrastructure
{
    public class StringDotNumberTextGenerator : IStringDotNumberTextGenerator
    {
        private readonly AppSettings _appSettings;
        private readonly Random _random = new Random();

        public StringDotNumberTextGenerator(IOptions<AppSettings> options)
        {
            _appSettings = options.Value;
           
        }

        public string CreateSentences(long size)
        {
            var words = _appSettings.File.RecordsData.AvailableWords;
            var delimiter = _appSettings.File.RecordsData.Delimiter;

            var mainStringBuilder = new StringBuilder();
            var currentSize = mainStringBuilder.Length;
            while (currentSize <= size)
            {
                var wordsCount = _random.Next(_appSettings.File.RecordsData.MinRecordWordsCount, _appSettings.File.RecordsData.MaxRecordWordsCount + 1);
                var stringBuilder = new StringBuilder();
                stringBuilder
                    .Append(_random.Next(0, 100000))
                    .Append(delimiter);

                for (var i = 0; i < wordsCount; i++)
                {
                    stringBuilder
                        .Append(words[_random.Next(words.Length)])
                        .Append(' ');
                }

                var sentence = stringBuilder.ToString().TrimEnd();
                sentence = char.ToUpper(sentence[0]) + sentence[1..];
                mainStringBuilder.AppendLine(sentence);

                currentSize = mainStringBuilder.Length;
            }

            return mainStringBuilder.ToString();
        }
    }
}
