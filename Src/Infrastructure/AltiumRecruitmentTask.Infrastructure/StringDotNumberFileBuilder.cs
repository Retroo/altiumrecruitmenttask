﻿using System.IO;
using System.Text;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Application.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace AltiumRecruitmentTask.Infrastructure
{
    public class StringDotNumberFileBuilder : IStringDotNumberFileBuilder
    {
        private readonly AppSettings _appSettings;
        private readonly IStringDotNumberTextGenerator _randomTextGenerator;

        public StringDotNumberFileBuilder(IOptions<AppSettings> appSettingsOptions, IStringDotNumberTextGenerator randomTextGenerator)
        {
            _appSettings = appSettingsOptions.Value;
            _randomTextGenerator = randomTextGenerator;
        }

        public void Build()
        {
            var requestedSizeBytes = _appSettings.RandomStringDotNumberFileBuilder.FileSizeBytes;
            var maxBatchSizeBytes = _appSettings.RandomStringDotNumberFileBuilder.MaxBatchSizeBytes;
            var calculatedBatchSizeBytes = requestedSizeBytes / 10;

            if (calculatedBatchSizeBytes > maxBatchSizeBytes) calculatedBatchSizeBytes = maxBatchSizeBytes;

            var filePath = Path.Combine(_appSettings.File.Directory, _appSettings.File.TestFileName);
            using var streamWriter = new StreamWriter(filePath, true, Encoding.UTF8, 1024 * 100)
            {
                AutoFlush = true
            };
            var sentences = _randomTextGenerator.CreateSentences(calculatedBatchSizeBytes);
            while (streamWriter.BaseStream.Length < requestedSizeBytes)
            {
                streamWriter.Write(sentences);
            }
        }
    }
}
