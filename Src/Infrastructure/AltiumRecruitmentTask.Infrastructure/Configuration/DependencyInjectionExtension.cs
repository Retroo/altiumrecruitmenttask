﻿using AltiumRecruitmentTask.Application.Common.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace AltiumRecruitmentTask.Infrastructure.Configuration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddInfrastructureDependencies(this IServiceCollection services)
        {
            AddTransient(services);

            return services;
        }

        private static void AddTransient(IServiceCollection services)
        {
            services.AddTransient<IStringDotNumberFileBuilder, StringDotNumberFileBuilder>();
            services.AddTransient<IStringDotNumberTextGenerator, StringDotNumberTextGenerator>();
            services.AddTransient<IStringDotNumberComparer, StringDotNumberComparer>();
        }
    }
}
