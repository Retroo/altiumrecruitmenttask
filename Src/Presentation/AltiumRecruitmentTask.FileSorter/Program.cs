﻿using System.IO;
using AltiumRecruitmentTask.Application;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Infrastructure.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AltiumRecruitmentTask.FileSorter
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            var externalMergeSorter = serviceProvider.GetService<ExternalMergeSorter>();
            externalMergeSorter?.Sort();

            serviceProvider.Dispose();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            services.AddApplicationDependencies();
            services.AddInfrastructureDependencies();
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
            services.AddSingleton<AppSettings>();
        }
    }
}
