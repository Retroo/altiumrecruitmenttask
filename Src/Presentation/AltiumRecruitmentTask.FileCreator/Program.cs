﻿using System.IO;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Application.Common.Interfaces;
using AltiumRecruitmentTask.Infrastructure.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AltiumRecruitmentTask.FileCreator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var services = new ServiceCollection();
            ConfigureServices(services);

            var serviceProvider = services.BuildServiceProvider();

            var randomStringDotNumberFileBuilder = serviceProvider.GetService<IStringDotNumberFileBuilder>();
            randomStringDotNumberFileBuilder?.Build();

            serviceProvider.Dispose();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            services.AddInfrastructureDependencies();
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
            services.AddSingleton<AppSettings>();
        }
    }
}
