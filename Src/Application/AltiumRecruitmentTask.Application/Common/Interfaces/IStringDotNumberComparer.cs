﻿namespace AltiumRecruitmentTask.Application.Common.Interfaces
{
    public interface IStringDotNumberComparer
    {
        int Compare(string stringA, string stringB);
    }
}
