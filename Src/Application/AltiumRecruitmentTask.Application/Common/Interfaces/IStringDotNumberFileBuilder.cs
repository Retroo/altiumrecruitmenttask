﻿namespace AltiumRecruitmentTask.Application.Common.Interfaces
{
    public interface IStringDotNumberFileBuilder
    {
        void Build();
    }
}
