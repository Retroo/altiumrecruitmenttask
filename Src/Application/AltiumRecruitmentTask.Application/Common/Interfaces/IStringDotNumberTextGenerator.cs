﻿namespace AltiumRecruitmentTask.Application.Common.Interfaces
{
    public interface IStringDotNumberTextGenerator
    {
        string CreateSentences(long size);
    }
}
