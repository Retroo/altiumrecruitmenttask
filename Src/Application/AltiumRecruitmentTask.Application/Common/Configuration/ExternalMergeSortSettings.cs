﻿namespace AltiumRecruitmentTask.Application.Common.Configuration
{
    public class ExternalMergeSortSettings
    {
        public int EstimatedRecordSizeBytes { get; set; }
        public int MaxMemoryUsageBytes { get; set; }
        public double RecordOverheadOfUsingQueue { get; set; }
        public int ChunkFileSizeBytes { get; set; }
    }
}