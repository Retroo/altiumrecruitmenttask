﻿namespace AltiumRecruitmentTask.Application.Common.Configuration
{
    public class FileSettings
    {
        public string Directory { get; set; }
        public string TestFileName { get; set; }
        public string ChunkFileName { get; set; }
        public string SortedChunkFileName { get; set; }
        public string SortedFileName { get; set; }
        public RecordsDataDto RecordsData { get; set; }
    }

    public class RecordsDataDto
    {
        public string[] AvailableWords { get; set; }
        public string Delimiter { get; set; }
        public int MinRecordWordsCount { get; set; }
        public int MaxRecordWordsCount { get; set; }
    }
}
