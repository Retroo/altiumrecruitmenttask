﻿using Microsoft.Extensions.DependencyInjection;

namespace AltiumRecruitmentTask.Application.Common.Configuration
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddApplicationDependencies(this IServiceCollection services)
        {
            AddTransient(services);

            return services;
        }

        private static void AddTransient(IServiceCollection services)
        {
            services.AddTransient<ExternalMergeSorter>();
        }
    }
}
