﻿namespace AltiumRecruitmentTask.Application.Common.Configuration
{
    public class AppSettings
    {
        public FileSettings File { get; set; }
        public ExternalMergeSortSettings ExternalMergeSort { get; set; }
        public RandomStringDotNumberFileBuilderSettings RandomStringDotNumberFileBuilder { get; set; }
    }

    public class RandomStringDotNumberFileBuilderSettings
    {
        public long FileSizeBytes { get; set; }
        public int MaxBatchSizeBytes { get; set; }
    }
}
