﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AltiumRecruitmentTask.Application.Common.Configuration;
using AltiumRecruitmentTask.Application.Common.Interfaces;
using Microsoft.Extensions.Options;

namespace AltiumRecruitmentTask.Application
{
    public class ExternalMergeSorter
    {
        private readonly IStringDotNumberComparer _stringDotNumberComparer;
        private readonly AppSettings _appSettings;

        public ExternalMergeSorter(IOptions<AppSettings> options, IStringDotNumberComparer stringDotNumberComparer)
        {
            _stringDotNumberComparer = stringDotNumberComparer;
            _appSettings = options.Value;
        }

        public void Sort()
        {
            SplitFileToChunks();
            SortDataInChunks();
            MergeTheChunks();
        }

        private void SplitFileToChunks()
        {
            var filePath = Path.Combine(_appSettings.File.Directory, _appSettings.File.TestFileName);
            if (!File.Exists(filePath))
            {
                throw new Exception($"File not exists: {filePath}");
            }

            var splittedFileNumber = 1;
            var fileName = $"{_appSettings.File.ChunkFileName}{splittedFileNumber}";

            var stringBuilder = new StringBuilder();
            using var streamReader = new StreamReader(filePath, Encoding.UTF8, true, 4096);
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                stringBuilder.AppendLine(line);

                if (stringBuilder.Length <= _appSettings.ExternalMergeSort.ChunkFileSizeBytes || streamReader.ReadLine() == null) continue;

                File.WriteAllText(Path.Combine(_appSettings.File.Directory, fileName), stringBuilder.ToString());
                stringBuilder = new StringBuilder();
                splittedFileNumber++;
                fileName = $"{_appSettings.File.ChunkFileName}{splittedFileNumber}";
            }

            if (stringBuilder.Length > 0)
            {
                File.WriteAllText(Path.Combine(_appSettings.File.Directory, fileName), stringBuilder.ToString());
            }
        }

        private void SortDataInChunks()
        {
            var delimiter = _appSettings.File.RecordsData.Delimiter;

            foreach (var path in Directory.GetFiles(_appSettings.File.Directory, $"{_appSettings.File.ChunkFileName}*"))
            {
                var contents = File.ReadAllLines(path);
                var orderedContents = contents
                    .OrderBy(x => x.Split(delimiter)[1])
                    .ThenBy(x => int.Parse(x.Split(delimiter)[0]))
                    .ToList();

                var newPath = path.Replace(_appSettings.File.ChunkFileName, _appSettings.File.SortedChunkFileName);
                File.WriteAllLines(newPath, orderedContents);
                File.Delete(path);
            }
        }

        private void MergeTheChunks()
        {
            var paths = Directory.GetFiles(_appSettings.File.Directory, $"{_appSettings.File.SortedChunkFileName}*");
            var chunksCount = paths.Length;
            var recordsCountInBuffer = CalculateRecordsCountInBuffer(chunksCount);

            var streamReadersArray = new StreamReader[chunksCount];
            for (var i = 0; i < chunksCount; i++)
            {
                streamReadersArray[i] = new StreamReader(paths[i]);
            }

            var queuesArray = new Queue<string>[chunksCount];
            for (var i = 0; i < chunksCount; i++)
            {
                queuesArray[i] = new Queue<string>(recordsCountInBuffer);
            }

            for (var i = 0; i < chunksCount; i++)
            {
                LoadQueue(queuesArray[i], streamReadersArray[i], recordsCountInBuffer);
            }

            var streamWriter = new StreamWriter(Path.Combine(_appSettings.File.Directory, $"{_appSettings.File.SortedFileName}"));
            while (true)
            {
                var lowestIndex = -1;
                var lowestValue = "";
                for (var i = 0; i < chunksCount; i++)
                {
                    if (queuesArray[i] == null) continue;

                    if (lowestIndex >= 0 && _stringDotNumberComparer.Compare(queuesArray[i].Peek(), lowestValue) >= 0) continue;

                    lowestIndex = i;
                    lowestValue = queuesArray[i].Peek();
                }

                if (lowestIndex == -1) break;

                streamWriter.WriteLine(lowestValue);
                queuesArray[lowestIndex].Dequeue();

                if (!IsQueueEmpty(queuesArray, lowestIndex)) continue;

                LoadQueue(queuesArray[lowestIndex], streamReadersArray[lowestIndex], recordsCountInBuffer);
                if (IsQueueEmpty(queuesArray, lowestIndex))
                {
                    queuesArray[lowestIndex] = null;
                }
            }
            streamWriter.Close();

            for (var i = 0; i < chunksCount; i++)
            {
                streamReadersArray[i].Close();
                File.Delete(paths[i]);
            }
        }

        private int CalculateRecordsCountInBuffer(int chunksCount)
        {
            var estimatedRecordSize = _appSettings.ExternalMergeSort.EstimatedRecordSizeBytes;
            var maxMemoryUsage = _appSettings.ExternalMergeSort.MaxMemoryUsageBytes;
            var bufferSize = maxMemoryUsage / chunksCount;
            var recordOverheadOfUsingQueue = _appSettings.ExternalMergeSort.RecordOverheadOfUsingQueue;
            var recordsCountInBuffer = (int)(bufferSize / estimatedRecordSize / recordOverheadOfUsingQueue);
            return recordsCountInBuffer;
        }

        private static bool IsQueueEmpty(Queue<string>[] queuesArray, int lowestIndex)
        {
            return queuesArray[lowestIndex].Count == 0;
        }

        private static void LoadQueue(Queue<string> queue, StreamReader file, int records)
        {
            for (var i = 0; i < records; i++)
            {
                if (file.Peek() < 0) break;
                queue.Enqueue(file.ReadLine());
            }
        }
    }
}
